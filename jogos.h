#pragma once
//#include "infojogo.h"
#include "infojogofutebol.h"
#include "infojogobasquetebol.h"
#include <string>
#include <vector>

//class Jogos : std::vector <std::unique_ptr<InfoJogo>>{
class Jogos {
public:
//  Jogos();
  void adicionarFut(std::unique_ptr<InfoJogoFutebol> valor);
  void adicionarBask(std::unique_ptr<InfoJogoBasquetebol> valor);
  void ordenaFut();
  void ordenaBask();
  void imprimeFut();
  void imprimeBask();
  //validar modalidade futebol ou basquetebol
  int selectModality();
  void menu();
//  using std::vector<std::unique_ptr<InfoJogo>>::push_back;

private:
  std::vector<std::unique_ptr<InfoJogoFutebol>> m_jogosFutebol;
  std::vector<std::unique_ptr<InfoJogoBasquetebol>> m_jogosBasquetebol;

//variaveis
  std::string m_namePlayer;
  std::string m_posPlayer;
  std::string m_nameTeamA;
  std::string m_nameTeamB;
  std::string m_players;
  std::string m_changes;
};

