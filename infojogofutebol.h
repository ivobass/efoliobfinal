#pragma once
#include "infojogo.h"
#include <string>

class InfoJogoFutebol : public InfoJogo {
public:
InfoJogoFutebol (std::string namePlayer, std::string posPlayer, int scorePlayer, int timePlayer,
                 std::string nameTeamA, std::string nameTeamB, int scoreTeamA, int scoreTeamB);

//Getters
    std::string getNamePlayer() const;
    std::string getPosPlayer() const;
    int getScorePlayer() const;
    int getTimePlayer() const;
    void imprimeJogo() const;
    void validar();
    void inputDataFut();

private:
  std::string m_namePlayer;
  std::string m_posPlayer;
  int m_scorePlayer;
  int m_timePlayer;
//  // variaveis para ler de dados
//   std::string namePlayer;
//   std::string posPlayer;
//   int scorePlayer;
//   int timePlayer;
//   std::string nameTeamA;
//   std::string nameTeamB;
//   int scoreTeamA;
//   int scoreTeamB;

};


