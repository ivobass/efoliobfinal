#include "infojogofutebol.h"
#include <iostream>
#include <string>
#include <algorithm>

void InfoJogoFutebol::inputDataFut() {
    // variaveis para ler de dados
    std::string namePlayer;
    std::string posPlayer;
    std::string nameTeamA;
    std::string nameTeamB;


    //interação com o utilizador
    std::cout << "Entre o nome da equipa A:" << std::endl;
    std::cin >> nameTeamA;
    std::cout << "Entre nome dos jugadores titulares da equipa A: \t" << nameTeamA << std::endl;
    // introduzir jugadores da equipa A:
//    for (int i = 1; i < 4; i++) {
        std::cout  << "\n";
        std::cout << "dados do jugador numero \t"  << std::endl;
        std::cout << "Entre o nome do jugador \t" << std::endl;
        std::cin >> namePlayer;
        std::cout  << "Posição do jugador (guarda-redes, defesa, médio, avançado):" << std::endl;;
        std::cin >> posPlayer;

//    }
    std::cout << "Entre o nome dos judagores suplentes da equipa A: " << std::endl;
    std::cout << std::endl;

//    for (int i = 1; i < 2; i++){

        std::cout << "Entre o nome do jugador"  << std::endl;
        std::cin >> namePlayer;
        std::cout << "Posição do jugador (guarda-redes, defesa, médio, avançado):" << std::endl;;
        std::cin >> posPlayer;
//    }
}

//construtor filho
InfoJogoFutebol::InfoJogoFutebol (std::string namePlayer, std::string posPlayer, int scorePlayer, int timePlayer,
                                  std::string nameTeamA, std::string nameTeamB, int scoreTeamA, int scoreTeamB)
    : InfoJogo(nameTeamA,nameTeamB,scoreTeamA,scoreTeamB)
{
    m_namePlayer = namePlayer;
    m_posPlayer = posPlayer;
    m_scorePlayer = scorePlayer;
    m_timePlayer = timePlayer;

}

//Getters
std::string InfoJogoFutebol::getNamePlayer() const{
    return m_namePlayer;
}

std::string InfoJogoFutebol::getPosPlayer() const{
    return m_posPlayer;
}
int InfoJogoFutebol::getScorePlayer() const{
    return m_scorePlayer;
}
int InfoJogoFutebol::getTimePlayer() const{
    return m_timePlayer;
}

void InfoJogoFutebol::imprimeJogo() const{
    std::cout << std::endl;
    std::cout << "Jugador: "<< m_namePlayer << " Posição: " << m_posPlayer << " Golos Marcados: " << m_scorePlayer
              << " Tempo em que marcou: " << m_timePlayer << std::endl;
}

//Setters
// valida jogadores suplentes


// Imprime resultados jogadores

