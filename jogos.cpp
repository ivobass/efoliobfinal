#include "jogos.h"
#include <iostream>
#include <algorithm>\


//valida jogadores titulares futebol
void Jogos::adicionarFut(std::unique_ptr<InfoJogoFutebol> valor) {


  m_jogosFutebol.push_back(std::move(valor));   // validar ou introduzir jogadores move indica ao compilador que ele não quer uma cópia, mas sim, mover o elemento.
}

//valida jogadores titulares basquetebol
void Jogos::adicionarBask(std::unique_ptr<InfoJogoBasquetebol>valor) {
  m_jogosBasquetebol.push_back(std::move(valor));
}

//ordena jogadores futebol
void Jogos::ordenaFut(){
    std::sort(std::begin(m_jogosFutebol),std::end(m_jogosFutebol));

}
//ordena jogadores Basketebol
void Jogos::ordenaBask(){
    std::sort(std::begin(m_jogosBasquetebol),std::end(m_jogosBasquetebol));

}
//lee os dados dos jugadores de futebol
void Jogos::imprimeFut() {
  for (auto& valoratual : m_jogosFutebol) {
    valoratual->imprimeJogo();
//    std::cout << "tamanho do vector" << m_jogosFutebol.size();
  }
  return;
}
//lee os dados dos jugadores de basketebol
void Jogos::imprimeBask() {
  for (auto& valoratual : m_jogosBasquetebol) {
    valoratual->imprimeJogo();
  }
  return;
}


//Menu principal
void Jogos::menu(){
       int op_menu=1;
    do {
        //        system("clear");
        std::cout << "====================================================" << std::endl;
        std::cout << "*       AF2 - 21093 - Programação por Objectos     *" << std::endl;
        std::cout << "====================================================" << std::endl;
        std::cout << "*                                                  *" << std::endl;
        std::cout << "*           ...Menu Principal...                   *" << std::endl;
        std::cout << "*                                                  *" << std::endl;
        std::cout << "*        1. Introduzir modalidade e dados do jogo  *" << std::endl;
        std::cout << "*        2. Informações Jogo Futebol               *" << std::endl;
        std::cout << "*        3. Informações Jogo Basquetebol           *" << std::endl;
        std::cout << "*        4. Listagem de Resultados de Futebol      *" << std::endl;
        std::cout << "*        5. Listagem de Resultados de Basquetebol  *" << std::endl;
        std::cout << "*        0. Sair                                   *" << std::endl;
        std::cout << "====================================================" << std::endl;
        std::cout << "Introduzir a sua opção:"<< std::endl;
        std::cin >> op_menu;

        } while(op_menu !=0);
}

//seleciona a modalidade
int Jogos::selectModality(){
    int modalidade =-1; //definimos uma opcao invalida

     //apresentamos o menu
    std::cout << "************** Escolha o tipo de Modalidade Futebol ou Basketbol **************" << std::endl;
    std::cout << "1 - Futebol" << std::endl;
    std::cout << "2 - Basketbol" << std::endl;
    std::cout << "3 - Voltar ao menu principal" << std::endl;

    //enquanto o utilizador nao quiser sair entramos num loop
    //para se sair tem que se escolher '3'
    do{
        std::cout << std::endl << "Opcao: "; //solicitamos a opcao
        std::cin >> modalidade;   //obtemos a opcao do input

        //se a opcao nao estiver no intervalo permitido
        //avisamos o utilizador e limpamos o buffer da entrada para evitar erros
        if(modalidade<1 || modalidade>3){
            std::cout << "Introduza uma opcao valida! Entre 1 e 2." << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }

    }while(modalidade<1 || modalidade>3);

    //limpamos o buffer da entrada para evitar erros
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return modalidade; //devolvemos a opcao escolhilda
}
